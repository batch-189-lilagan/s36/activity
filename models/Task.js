const mongoose = require('mongoose');

// Create a schema for the Task
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: "pending"
    }
});

// Export this schema into a model
module.exports = mongoose.model('Task', taskSchema);