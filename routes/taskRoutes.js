const express = require('express');

// Import the controllers
const TaskController = require('../controllers/TaskController.js');

// Initialize our router to be able to use express routing capabilities
const router = express.Router();

// Get all tasks
router.get('/', (request, response) => {
    TaskController.getAllTasks().then((tasks) => response.send(tasks));
});

// Add a task
router.post('/create', (request, response) => {
    TaskController.createTask(request.body).then((tasks) => response.send(tasks));
});

// Get 1 task by Id
router.get('/:id', (request, response) => {
    TaskController.getOneTask(request.params.id).then((result) => response.send(result));
});

// Update the status of 1 task
router.put('/:id/complete', (request, response) => {
    TaskController.updateStatusOfTask(request.params.id, request.body.status).then((result) => response.send(result));
});






module.exports = router;