const Task = require('../models/Task.js');

// Export the module needed for taskRoutes
// Getting all the tasks
module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result
    })
};

// Adding a single task
module.exports.createTask = (requestBody) => {
    let newTask = new Task({
        name: requestBody.name
    })

    return newTask.save().then((savedTask, error) => {
        if (error) {
            return error;
        }

        return 'Task created successfully!';
    })
};

// Get specific task based on Id
module.exports.getOneTask = ((taskId) => {

    return Task.findById(taskId).then((taskFound, error) => {

        if (error) {
            return error;
        }

        if (taskFound == null) {
            return `No result found`;
        }

        return taskFound;
    })

})

// Update specific task based on Id
module.exports.updateStatusOfTask = ((taskId, newStatus) => {

    return Task.findById(taskId).then((result, error) => {

        if (error) {
            return error
        }

        result.status = newStatus;

        return result.save().then((updatedTask, error) => {

            if (error) {
                return error
            }

            return updatedTask;
        })

    })
})